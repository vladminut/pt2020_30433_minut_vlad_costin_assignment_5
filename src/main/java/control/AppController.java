package control;

import logic.DataProcessing;
import logic.FileWriter;
import logic.MonitoredData;

import java.util.List;

public class AppController {

    private DataProcessing monitoredDataProcessing ;
    private FileWriter fileWriter;

    public void start(){
        monitoredDataProcessing = new DataProcessing();
        fileWriter = new FileWriter();
        List<MonitoredData> activities = monitoredDataProcessing.readActivities();

        int nbDays = monitoredDataProcessing.taskOne(activities);

        fileWriter.writeNumberOfDays(nbDays);
        fileWriter.writeCountActivities(monitoredDataProcessing.taskTwo(activities));
        fileWriter.writeCountActivitiesPerDay(monitoredDataProcessing.taskThree(activities));
        fileWriter.writeTotalDurationActivity(monitoredDataProcessing.totalDurationActivity(activities));

    }

}
