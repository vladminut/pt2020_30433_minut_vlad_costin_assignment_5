package logic;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataProcessing {

    List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();

    public List<MonitoredData> readActivities(){
        try{
            Stream<String> file = Files.lines(Paths.get("Activities.txt"));
            file.forEach(s->{
                String format = "yyy-MM-dd HH:mm:ss";
                DateFormat dateFormat = new SimpleDateFormat(format);
                String regex= "\t\t";
                String[] split = s.split(regex);
                Date start, end;
                String activity_label;
                try{
                    start = dateFormat.parse(split[0]);
                    end = dateFormat.parse(split[1]);
                    activity_label = split[2];
                    monitoredData.add(new MonitoredData(start, end, activity_label));
                } catch (ParseException e){
                    e.printStackTrace();
                }
            });
            file.close();
        } catch (IOException e){
            e.printStackTrace();

        }
        return monitoredData;
    }

    public int taskOne(List<MonitoredData> monitoredData){
        Set<Integer> distinctDays =monitoredData.stream().map(m->m.getStart_time().getDate()).collect(Collectors.toSet());
        return distinctDays.size();
    }

    public Map<String, Integer> taskTwo(List<MonitoredData>  monitoredDatas){
        Map<String, Integer> activities = monitoredDatas.stream()
                .collect(Collectors.groupingBy(
                        p -> p.getActivity_label(),
                        Collectors.reducing(0, e -> 1, Integer::sum)));
        System.out.println(activities);
        return activities;
    }

    public Map<Integer, Map<String, Integer>>taskThree(List<MonitoredData>  monitoredDatas){
        Map<Integer, Map<String, Integer>> actZile = monitoredDatas
                .stream()
                .collect(Collectors.groupingBy(m -> m.getStart_time().getDate(),
                        Collectors.groupingBy(s->s.getActivity_label(), Collectors.reducing(0, e -> 1, Integer::sum))));

        System.out.println(actZile);
        System.out.println("");
        return actZile;

    }

    public Map<String, Integer> totalDurationActivity(List<MonitoredData>  monitoredDatas){
        Map<String, Integer> durataTotala = monitoredDatas.stream().collect(
                Collectors.groupingBy(m -> m.getActivity_label(), Collectors.reducing(0,s->{
                    int  diferenta = s.getDuration();
                    return diferenta;
                },Integer::sum)));
        return durataTotala;
    }

}
