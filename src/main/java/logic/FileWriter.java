package logic;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class FileWriter {


    public void writeNumberOfDays(int number) {
        BufferedWriter outputStream = null;
        try {
            outputStream = new BufferedWriter(new java.io.FileWriter("task1.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.write("Number of distinct days in the monitoring data is");
            outputStream.newLine();
            outputStream.newLine();
            outputStream.write(Integer.toString(number));
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }


    public void writeCountActivities(Map<String, Integer> distinctActivity) {
        BufferedWriter outputStream = null;
        try {
            outputStream = new BufferedWriter(new java.io.FileWriter("task2.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.write("Here is how many times each activity appeared over the whole period");
            outputStream.newLine();
            for (String activity : distinctActivity.keySet()) {
                outputStream.write(activity);
                outputStream.write(" appears: ");
                outputStream.write(Integer.toString(distinctActivity.get(activity)));
                outputStream.write(" times");
                outputStream.newLine();

            }
            outputStream.newLine();
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void writeCountActivitiesPerDay(Map<Integer, Map<String, Integer>> activitiesPerDay) {
        BufferedWriter outputStream = null;
        try {
            outputStream = new BufferedWriter(new java.io.FileWriter("task3.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.write("Here is how many times each activity appeared per day: ");
            outputStream.newLine();
            outputStream.newLine();
            for (Integer day : activitiesPerDay.keySet()) {
                outputStream.newLine();
                outputStream.write("Day "+ Integer.toString(day));
                outputStream.newLine();
                for (String activity : activitiesPerDay.get(day).keySet()) {
                    outputStream.write("   "+activity);
                    outputStream.write(" appears: ");
                    outputStream.write(Integer.toString(activitiesPerDay.get(day).get(activity)));
                    outputStream.write(" times");
                    outputStream.newLine();
                }

            }
            outputStream.newLine();
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    public void writeTotalDurationActivity(Map<String, Integer> activitiesTotalTime) {
        BufferedWriter outputStream = null;
        try {
            outputStream = new BufferedWriter(new java.io.FileWriter("task4.txt"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.write("Here are the distinct activities and their duration: ");
            outputStream.newLine();
            outputStream.newLine();
            for (String activity : activitiesTotalTime.keySet()) {
                outputStream.write( "  "+activity+ " takes: " );
                outputStream.write(Long.toString(TimeUnit.MILLISECONDS.toHours(activitiesTotalTime.get(activity)))+" Hours/");
                outputStream.write(Long.toString(TimeUnit.MILLISECONDS.toMinutes(activitiesTotalTime.get(activity)))+" Minutes/");
                outputStream.write(Long.toString(TimeUnit.MILLISECONDS.toSeconds(activitiesTotalTime.get(activity))) + " Seconds ");
                outputStream.newLine();

            }
            outputStream.newLine();
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
