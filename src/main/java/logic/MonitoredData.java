package logic;
import java.util.Date;

public class MonitoredData {

        private Date start_time;
        private Date end_time;
        private String activity_label;

    public MonitoredData(Date start_time, Date end_time, String activity_label) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity_label = activity_label;
    }

    public Date getStart_time() {
        return start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public String getActivity_label() {
        return activity_label;
    }

    public int getDuration(){
        return (int)this.getEnd_time().getTime() - (int)this.getStart_time().getTime();
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "start_time=" + start_time +
                ", end_time=" + end_time +
                ", activity_label='" + activity_label + '\'' +
                '}';
    }
}
